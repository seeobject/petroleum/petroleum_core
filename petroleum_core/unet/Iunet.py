import torch
from .unet import *
from torchvision import transforms
from . import custom_transforms as tr
from PIL import Image
from .utils import  *
from torchvision.utils import make_grid

class IUnet:
    def __init__(self, model_path, n_channels=3, n_classes=10, gpu_id=0):
        self.__device = torch.device(
            "cuda:%s" % gpu_id if torch.cuda.is_available() else "cpu")

        self.__net = Unet(n_channels=n_channels, n_classes=n_classes)

        assert(model_path)
        self.__net.load_state_dict(torch.load(model_path, map_location="cpu")['state_dict'])
        self.__net.cuda(device=self.__device)
        self.__net.eval()

        self.__transforms = transforms.Compose([
        tr.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
        tr.ToTensor()])

    def detect(self, img_path):
        image = Image.open(img_path).convert('RGB')

        target = Image.open(img_path).convert('L')
        sample = {'image': image, 'label': target}
        tensor_in = self.__transforms(sample)['image'].unsqueeze(0)
        tensor_in = tensor_in.cuda(device=self.__device) if torch.cuda.is_available() else tensor_in
        with torch.no_grad():
            output = self.__net(tensor_in)

        mask = make_grid(decode_seg_map_sequence(torch.max(output[:3], 1)[1].detach().cpu().numpy()),
                                3, normalize=False, range=(0, 9))
        mask = mask.mul(255).add_(0.5).clamp_(0, 255).permute(1, 2, 0).to('cpu', torch.uint8).numpy()
        mask = np.concatenate([mask[:, :, 2:], mask[:,:,1:2], mask[:,:,0:1]], axis=2)

        return mask 
