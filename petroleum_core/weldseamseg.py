import os
import cv2
from matplotlib.pyplot import contour
from .unet import IUnet
import numpy as np

class IWeldSeamSeg:
    def __init__(self, modelDir, gpu_id=0):
        modelDir = modelDir + '/' if not modelDir.endswith('/') else modelDir
        model_path = modelDir+"petroleum/weldseamseg.pt"
        self.__net = IUnet(model_path,n_channels=3, n_classes=10, gpu_id=gpu_id)

    def union_image_mask(self, mask):
        result = []
        _, thresh = cv2.threshold(mask, 40, 255, 0)
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for index, cnt in enumerate(contours):  
            info = {}

            fill_image = cv2.fillConvexPoly(np.zeros(mask.shape), contours[index], 255)
            info["area"] = cv2.countNonZero(fill_image)

            rect = cv2.minAreaRect(np.array(cnt))  #最小外接矩形
            info["long_axis"], info["short_axis"] = max(rect[1][:2]), min(rect[1][:2])

            if min(rect[1][:2])<1:
                continue
            info["axis_ratio"] = max(rect[1][:2])/min(rect[1][:2])

            info["perimeter"] = cv2.arcLength(cnt,True)

            M = cv2.moments(cnt)
            info["center"] = (int(M['m10']/M['m00']), int(M['m01']/M['m00']))

            result.append(info)

        return result
        

    def detect(self, img_path):
        mask = self.__net.detect(img_path)
        result = self.union_image_mask(cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY))

        return result ,mask

def run():
    modelDir = r'/home/zqp/gitlab/models/'
    net = IWeldSeamSeg(modelDir, 0)

    picDir = r'./testimage1/'
    for picName in os.listdir(picDir):
        im = cv2.imread(picDir+picName)
        result = net.detect(picDir+picName)
    
        cv2.imshow('im', result)
        if cv2.waitKey(0) == 27:
            break


if __name__ == '__main__':
    run()