import cv2
import numpy as np
import math
import time
import os

def stretchImage(data, s=0.005, bins = 2000):    #线性拉伸，去掉最大最小0.5%的像素值，然后线性拉伸至[0,1]
    ht = np.histogram(data, bins)
    d = np.cumsum(ht[0])/float(data.size)
    lmin = 0; lmax=bins-1
    while lmin<bins:
        if d[lmin]>=s:
            break
        lmin+=1
    while lmax>=0:
        if d[lmax]<=1-s:
            break
        lmax-=1
    return np.clip((data-ht[1][lmin])/(ht[1][lmax]-ht[1][lmin]), 0,1)
 
g_para = {}
def getPara(radius = 5):                        #根据半径计算权重参数矩阵
    global g_para
    m = g_para.get(radius, None)
    if m is not None:
        return m
    size = radius*2+1
    m = np.zeros((size, size))
    for h in range(-radius, radius+1):
        for w in range(-radius, radius+1):
            if h==0 and w==0:
                continue
            m[radius+h, radius+w] = 1.0/math.sqrt(h**2+w**2)
    m /= m.sum()
    g_para[radius] = m
    return m
 
def zmIce(I, ratio=4, radius=300):                     #常规的ACE实现
    para = getPara(radius)
    height,width = I.shape
    zh,zw = [0]*radius + [x for x in range(height)] + [height-1]*radius, [0]*radius + [x for x in range(width)]  + [width -1]*radius
    Z = I[np.ix_(zh, zw)]
    res = np.zeros(I.shape)
    for h in range(radius*2+1):
        for w in range(radius*2+1):
            if para[h][w] == 0:
                continue
            res += (para[h][w] * np.clip((I-Z[h:h+height, w:w+width])*ratio, -1, 1))
    return res
 
def zmIceFast(I, ratio=3, radius=2):                #单通道ACE快速增强实现
    height, width = I.shape[:2]
    if min(height, width) <=2:
        return np.zeros(I.shape)+0.5
    Rs = cv2.resize(I, ((width+1)//2, (height+1)//2))
    Rf = zmIceFast(Rs, ratio, radius)             #递归调用
    Rf = cv2.resize(Rf, (width, height))
    Rs = cv2.resize(Rs, (width, height))
 
    return Rf+zmIce(I,ratio, radius)-zmIce(Rs,ratio,radius)    
            
def zmIceColor(I, ratio=4, radius=3):               #rgb三通道分别增强，ratio是对比度增强因子，radius是卷积模板半径
    res = np.zeros(I.shape)
    for k in range(3):
        res[:,:,k] = stretchImage(zmIceFast(I[:,:,k], ratio, radius))
    return res


def ace_enhance_gray(img_path):
    imagesize =  cv2.resize(cv2.imread(img_path,0), (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_NEAREST) 
    m = zmIceFast(imagesize/255.0)*255
    img = cv2.resize(m, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)

    return img

def ace_enhance_apply_color(img_path):
    blur = cv2.GaussianBlur(cv2.imread(img_path, 0), (9, 9), 0)
    img_color = cv2.applyColorMap(blur, cv2.COLORMAP_JET)

    return img_color

def ace_enhance(img_path):
    # img = cv2.bitwise_not(cv2.imread(img_path,0))

    imagesize =  cv2.resize(img, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_NEAREST) 
    m = zmIceFast(imagesize/255.0)*255
    img = cv2.resize(m, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)
    img = img.astype(np.uint8)

    blur = cv2.GaussianBlur(img, (9, 9), 0)
    img_color = cv2.applyColorMap(blur, cv2.COLORMAP_JET)

    return img_color

 
if __name__ == '__main__':

    picDir = r'./testimage/'
    for picName in os.listdir(picDir):
        im = cv2.imread(picDir+picName)
        result = ace_enhance(picDir+picName)
    
        cv2.imshow('im', im)
        cv2.imshow('imenhance', result)
        if cv2.waitKey(0) == 27:
            break
        #cv2.waitKey(0)  """