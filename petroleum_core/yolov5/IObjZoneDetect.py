import os
import cv2
import torch
from .models.yolo import Detect, Model
import torch.nn as nn
import numpy as np
import time
from .models.general import non_max_suppression, scale_coords


def autopad(k, p=None):  # kernel, padding
    if p is None:
        p = k // 2 if isinstance(k, int) else [x // 2 for x in k]  # auto-pad
    return p


class Conv(nn.Module):
    # Standard convolution
    # ch_in, ch_out, kernel, stride, padding, groups
    def __init__(self, c1, c2, k=1, s=1, p=None, g=1, act=True):
        super(Conv, self).__init__()
        self.conv = nn.Conv2d(c1, c2, k, s, autopad(k, p),
                              groups=g, bias=False)
        self.bn = nn.BatchNorm2d(c2)
        self.act = nn.SiLU() if act is True else (
            act if isinstance(act, nn.Module) else nn.Identity())

    def forward(self, x):
        return self.act(self.bn(self.conv(x)))

    def fuseforward(self, x):
        return self.act(self.conv(x))


class IObjZoneYOLOV5Detect:
    def __init__(self, model_path, gpu_id=0):
        self.__device = torch.device(
            "cuda:%s" % gpu_id if torch.cuda.is_available() else "cpu")
        assert(os.path.exists(model_path), model_path + " not exist")
        ckpt = torch.load(model_path, map_location=self.__device)  # load
        model = ckpt["ema"].float().fuse().eval()
        for m in model.modules():
            if type(m) in [nn.Hardswish, nn.LeakyReLU, nn.ReLU, nn.ReLU6, nn.SiLU, Detect, Model]:
                m.inplace = True  # pytorch 1.7.0 compatibility
            elif type(m) is Conv:
                m._non_persistent_buffers_set = set()  # pytorch 1.6.0 compatibility

        self.__imgsz = 640
        self.__net = model
        if torch.cuda.is_available():
            self.__net.half()
            data = torch.zeros(1, 3, self.__imgsz, self.__imgsz).to(
                self.__device).type_as(next(self.__net.parameters()))
            self.__net(data)

    def letterbox(self, img, new_shape=(640, 640), color=(114, 114, 114), stride=32):
        shape = img.shape[:2]  # current shape [height, width]

        r = min(new_shape[0] / shape[0], new_shape[1] / shape[1])

        # Compute padding
        ratio = r, r  # width, height ratios
        new_unpad = int(round(shape[1] * r)), int(round(shape[0] * r))
        dw, dh = new_shape[1] - new_unpad[0], new_shape[0] - \
            new_unpad[1]  # wh padding
        dw, dh = np.mod(dw, stride), np.mod(dh, stride)  # wh padding

        dw /= 2  # divide padding into 2 sides
        dh /= 2

        if shape[::-1] != new_unpad:  # resize
            img = cv2.resize(img, new_unpad, interpolation=cv2.INTER_LINEAR)
        top, bottom = int(round(dh - 0.1)), int(round(dh + 0.1))
        left, right = int(round(dw - 0.1)), int(round(dw + 0.1))
        img = cv2.copyMakeBorder(
            img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)  # add border

        img = img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
        img = np.ascontiguousarray(img)

        return img

    @staticmethod
    def boxConvert(b, w, h):
        x1, y1, x2, y2 = b

        x1 = 0 if x1 < 0 else int(x1)
        y1 = 0 if y1 < 0 else int(y1)
        x2 = w if x2 > w else int(x2)
        y2 = h if y2 > h else int(y2)

        return (x1, y1, x2, y2)

    def detect(self, img_bgr, thresh=0.45):
        img = self.letterbox(img_bgr)
        img = torch.from_numpy(img).to(self.__device)
        if torch.cuda.is_available():
            img = img.half()
        img = (img/255.0).unsqueeze(0)
        pred = self.__net(img, augment=False)[0]
        pred = non_max_suppression(pred, thresh, iou_thres=0.15, max_det=1000)[0]

        pred[:, :4] = scale_coords(
            img.shape[2:], pred[:, :4], img_bgr.shape).round()
        pred = pred.cpu().numpy().tolist()

        result = []
        h, w = img_bgr.shape[:2]
        for box in pred:
            temp = {}
            b = (box[0], box[1], box[2], box[3])
            temp["zone"] = self.boxConvert(b, w, h)
            temp["cls"] = int(box[-1])
            temp["score"] = box[-2]
            result.append(temp)

        return result


def addRectangle(im, boxes):
    font = cv2.FONT_HERSHEY_SIMPLEX
    for box in boxes:
        zone = box["zone"]
        cv2.rectangle(im, (zone[0], zone[1]),
                      (zone[2], zone[3]), (0, 0, 255), 2)
        cv2.putText(im, "%s:%.2f" % (
            box["cls"], box["score"]), (zone[0], zone[1]), font, 1, (0, 255, 0))


def run():
    model_file = "/home/zqp/gitlab/models/vehicle/cartwheel.pth"
    detector = IObjZoneYOLOV5Detect(model_file)

    pic_dir = "/home/zqp/testpic/cartwheel/"

    cv2.namedWindow("img", 0)
    for picname in sorted(os.listdir(pic_dir)):
        pic_path = pic_dir+picname
        img = cv2.imread(pic_path)

        start = time.time()
        boxes = detector.detect(img)
        end = time.time()
        print("detect cost time: %s ms" % ((end-start)*1000))
        addRectangle(img, boxes)
        cv2.imshow("img", img)
        cv2.waitKey(0)


if __name__ == "__main__":
    run()
