from setuptools import setup, find_packages
# package = find_packages()

setup(name='fengdian_core',
      version='1.0.2',
      author="zqp",
      author_email="zqp@qq.com",
      url="https://gitlab.com/seeobject/fengdian/fengdian_core.git",
      packages=find_packages(),
      install_requires=["opencv-python"],
      zip_safe=False
      )
