import os
import time
import cv2
from petroleum_core import IWeldSeamSeg, addRectangle

if __name__ == "__main__":
    model_dir = "/home/zqp/gitlab/models/"
    detector = IWeldSeamSeg(model_dir)
    picdir = "/home/zqp/testpic/weldseamseg/"
    pic_names = os.listdir(picdir)

    for picname in pic_names:
        print(picname)
        if not picname.endswith(".png"):
            continue

        start = time.time()
        result, mask = detector.detect(picdir + picname)
        end = time.time()

        print(result)
        print("detect cost time %s ms" % ((end - start) * 1000))
        cv2.imshow("im", mask)
        cv2.waitKey(0)
