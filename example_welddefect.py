import os
import time
import cv2
from petroleum_core import IWelddefect, addRectangle

if __name__ == "__main__":
    model_dir = "/home/zqp/gitlab/models/"
    detector = IWelddefect(model_dir)
    picdir = "/home/zqp/testpic/welddefect/"
    pic_names = os.listdir(picdir)

    for picname in pic_names:
        print(picname)
        if not picname.endswith(".jpg"):
            continue

        start = time.time()
        boxes = detector.detect(picdir + picname, is_src=False)
        end = time.time()

        im = cv2.imread(picdir+picname)
        addRectangle(im, boxes)

        print(boxes)
        print("detect cost time %s ms" % ((end - start) * 1000))
        cv2.imshow("im", im)
        cv2.waitKey(0)
