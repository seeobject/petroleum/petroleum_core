import os
import time
import cv2
from petroleum_core import ace_enhance_apply_color, ace_enhance_gray

if __name__ == '__main__':
    picDir = r'./testimage/'
    for picName in os.listdir(picDir):
        print(picName)
        im = cv2.imread(picDir+picName)
        gray = ace_enhance_gray(picDir+picName)
        
        cv2.imwrite("./test.jpg", gray)
        result = ace_enhance_apply_color("./test.jpg")

    
        cv2.imshow('im', im)
        cv2.imshow('imenhance', result)
        if cv2.waitKey(0) == 27:
            break
        #cv2.waitKey(0)  """